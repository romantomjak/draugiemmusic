Draugiem.lv mūziķu kompozīciju vilknis. Pēc lejupielādes failu ieteicams augšupielādēt zamzar.com, lai pārveidotu uz mp3 formātu vai arī atspiest izmantojot Moitah FLV extractor.

1. Nokačājam http://moitah.net/download/latest/FLV_Extract.zip
2. Atzipojam "FLVExtract.exe" un palaižam to.
3. Izņemam ķeksi no "Video" un "Timecodes".
4. Iezīme visus savus FLV un ievelc iekš programmas & viss!
/*
 * DraugiemMusic
 *
 * Content script that modifies page by adding download button
 * and div containing links to all of the musician's songs.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want To
 * Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * Author: r00m (http://twitter.com/r00m)
 * Email: r.tomjaks@gmail.com
 */

self.port.on("pageLoaded", function() {
	console.info("pageLoaded");

	// make sure we're in right place and if so, add download link
	if($(".picture img[src*='/music/']").length > 0) {
		console.info("#player found");

		$(".sideMenu li:nth-child(9)").after('<li id="draugiemMusic"><div class="musicIcon"><a href="javascript:;">Dziesmu lejupielāde</a></div></li>');
		
		$(".sideMenu :nth-child(10)").on("click", function(e) {
			console.info("clicked on link");

			$("#ct").empty();
			$("#ct").html("<h1>Dziesmu lejupielāde</h1>"
               + "<div class=\"postText\" id=\"songList\">"
               + "Pēc lejupielādes failu ieteicams augšupielādēt <a href=\"http://zamzar.com/\" target=\"_blank\">zamzar.com</a>, "
               + "lai pārveidotu uz mp3 formātu vai arī atspiest izmantojot Moitah FLV extractor."
               + "<ol style=\"padding-left:40px; margin:13px 0;\">"
               + "<li>Nokačājam <a href=\"http://moitah.net/download/latest/FLV_Extract.zip\">http://moitah.net/download/latest/FLV_Extract.zip</a></li>"
               + "<li>Atzipojam \"FLVExtract.exe\" un palaižam to.</li>"
               + "<li>Izņemam ķeksi no \"Video\" un \"Timecodes\".</li>"
               + "<li>Iezīme visus savus FLV un ievelc iekš programmas & viss!</li>"
               + "</ol>"
               + "NB: <a href=\"http://www.winamp.com/\" target=\"_blank\">Winamp</a> (5.5+ ?) un <a href=\"http://www.videolan.org/\" target=\"_blank\">VLC</a> (0.8.8+ ?) spēj atskaņot FLV failus!"
               + "</div");

			self.port.emit("buttonAttached");
		});
	}
});

self.port.on("createSongDiv", function() {
	console.info("createSongDiv");

	// set active menu
	$(".sideMenu li").removeClass("activeMenu");
	$("#draugiemMusic").addClass("activeMenu");

	var musicianID = $(".picture img").attr("src").match(/nm_(\d+)\.jpg/, "g")[1];

	console.info("musicianID = " + musicianID);

	$.getJSON("http://www.draugiem.lv/music/rq/get.php?task=get_playlist&pid=0&type=artist&objectid=" + musicianID, function(data) {
		var songs = [];

		$.each(data, function(key, val) {
			songs.push('<li><a href="' + val.song_url + '" title="Lejupielādēt <strong>' + val.title + '</strong>" onclick="this.setAttribute(\'title\', \'' + val.artist_name + " - " + val.title + '\');  return false;">' + val.title + '</a></li>');
		})

		$("<ol/>", {
			"class": "postText",
			style: "list-style-type:decimal;margin:13px 0 0 25px;",
			html: songs.join(''),
			click: function(e) {
				e.preventDefault();

				if (e.target.href)
					self.port.emit("songLinkClicked", e.target.href, e.target.title);
			}
		}).appendTo("#songList");
	});
});

self.port.on("translateFileName", function(fileURI, baseFileName) {
	console.info("translateFileName");

 	baseFileName = baseFileName.toLowerCase();

	// ascii translation
	var replace = new Array("ā","č","ē","ģ","ī","ķ","ļ","ņ","š","ū","ž","а","б","в","г","д","е","ё","ж","з","и","й","к","л","м","н","о","п","р","с","т","у","ф","х","ц","ч","ш","щ","ы","ь","э","ю","я");
	var by = new Array("aa","ch","ee","gj","ii","kj","lj","nj","sh","uu","zh","a","b","v","g","d","e","yo","zh","z","i","j","k","l","m","n","o","p","r","s","t","u","f","h","c","ch","sh","w","y","'","je","yu","ya");
	for(var i=0; i<replace.length; i++) {
	 	baseFileName = baseFileName.replace(new RegExp(replace[i], "g"), by[i]).replace(/^\s\s*/g, "").replace(/\s\s*$/g, "");;
	}

	// special char replace
 	baseFileName = baseFileName.replace(/\s/g, '_').replace(/[!@#\$%\^&\*\[\]\+\<\>\;\.\?\{\}\\\/\:\"\'\|\~\`\,]/g, '');

	self.port.emit("openSaveDialog", fileURI, baseFileName);
});

/*
 * DraugiemMusic
 *
 * Modifies page source to add a button that allows user to download
 * musician's FLV files.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want To
 * Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * Author: r00m (http://twitter.com/r00m)
 * Email: r.tomjaks@gmail.com
 */

var { Cc, Ci } = require("chrome");

var pageMod = require("sdk/page-mod");
var self = require("sdk/self");
var fileDialog = require("file-dialog");
var windowUtils = require("sdk/window/utils");

pageMod.PageMod({
	include: "http://www.draugiem.lv/*",

	contentScriptFile: [self.data.url("jquery-1.9.0.min.js"),
						self.data.url("draugiem-mod.js")],

	contentScriptWhen: "ready", // DOMContentLoaded

	onAttach: function(worker) {
		worker.port.emit("pageLoaded");

		worker.port.on("buttonAttached", function() {
			worker.port.emit("createSongDiv");
		})

		worker.port.on("songLinkClicked", function(href, title) {
			worker.port.emit("translateFileName", href, title);
		})

		worker.port.on("openSaveDialog", function(fileURI, baseFileName) {
			console.info("openSaveDialog");

			var file = fileDialog.openDialog(baseFileName + ".flv");

			if (file !== null) {
				console.info("fileName = " + file.path);

				try {
					// create persistence object
					const nsIWBP = Ci.nsIWebBrowserPersist;
                    var webBrowserPersist = Cc["@mozilla.org/embedding/browser/nsWebBrowserPersist;1"]
                    							.createInstance(nsIWBP);

					// override default persist flags
     				webBrowserPersist.persistFlags = nsIWBP.PERSIST_FLAGS_REPLACE_EXISTING_FILES |
			     										nsIWBP.PERSIST_FLAGS_BYPASS_CACHE |
			     										nsIWBP.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;

					// create URI's
     				var ioService = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

    				var remoteFileURI = ioService.newURI(fileURI, null, null); 
					var localFileURI = ioService.newFileURI(file);

					// get current window privacy context
					var privacyContext = windowUtils.getWindowLoadingContext(windowUtils.getMostRecentBrowserWindow());

					// create download and then start it
					var downloadManager = Cc["@mozilla.org/download-manager;1"].getService(Ci.nsIDownloadManager);

                  	webBrowserPersist.progressListener = downloadManager.addDownload(
                  		Ci.nsIDownloadManager.DOWNLOAD_TYPE_DOWNLOAD,
                  		remoteFileURI,
                  		localFileURI,
                  		baseFileName + ".flv",
                  		null,
                  		Math.round(Date.now() * 1000),
                  		null,
                  		webBrowserPersist,
                  		false);

	    			webBrowserPersist.saveURI(remoteFileURI, null, null, null, "", file, privacyContext);
				}
				catch(e) {
					console.info("error: " + e);
				}
				
			} else {
				console.info("file download canceled");
			}
		})
	}
});
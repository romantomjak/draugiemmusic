/*
 * DraugiemMusic
 *
 * Shows file picker dialog for saving files to disk.
 *
 * Based on: https://github.com/swiperthefox/file-dialog
 * See: https://developer.mozilla.org/en-US/docs/XPCOM_Interface_Reference/nsIFilePicker
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want To
 * Public License, Version 2, as published by Sam Hocevar. See
 * http://sam.zoy.org/wtfpl/COPYING for more details.
 *
 * Author: r00m (http://twitter.com/r00m)
 * Email: r.tomjaks@gmail.com
 */

var { Cc, Ci } = require("chrome");
var utils = require("sdk/window/utils");

const nsIFilePicker = Ci.nsIFilePicker;

/*
 * Module parameters:
 * baseFileName - Default file name that gets displayed when dialog is opened
 *
 * return value:
 * Instance of nsILocalFile or null if canceled.
 */
module.exports.openDialog = function(baseFileName) {
  var fp = Cc["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

  // initialize
  fp.defaultString = baseFileName; // default dialog file name + extension
  fp.init(utils.getMostRecentBrowserWindow(), "Ievadiet faila nosaukumu", nsIFilePicker.modeSave);

  // setup filter
  fp.appendFilter("FLV file", "flv");

  // show the file dialog
  var rv = fp.show();

  // return nsILocalFile
  if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace)
    return fp.file;

  // user pressed cancel, etc - return null
  return null;
};
